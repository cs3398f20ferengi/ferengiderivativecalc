package ferengiCalc;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JEditorPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class statCalculator extends customAbstract {

	JFrame statCalcFrame;
	private JTextField input;
	private JTextField meanText;
	private JTextField modeText;
	private JTextField medianText;
	private JTextField rangeText;
	statCompute sC = new statCompute();
	
	/**
	 * Launch the application.
	 */
	public static void statCalc() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					statCalculator window = new statCalculator();
					window.statCalcFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public statCalculator() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		statCalcFrame = new JFrame();
		statCalcFrame.getContentPane().setBackground(windowColor1);
		statCalcFrame.setTitle("Statistic Calculator");
		statCalcFrame.setBounds(100, 100, 326, 365);
		statCalcFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		statCalcFrame.getContentPane().setLayout(null);
		
		//Labels
		JLabel enterDataLabel = new JLabel("Enter Data Set:");
		enterDataLabel.setForeground(fontColor);
		enterDataLabel.setBounds(6, 6, 118, 16);
		statCalcFrame.getContentPane().add(enterDataLabel);
		
		JLabel meanLabel = new JLabel("Mean:");
		meanLabel.setForeground(fontColor);
		meanLabel.setBounds(6, 62, 61, 16);
		statCalcFrame.getContentPane().add(meanLabel);
		
		JLabel ModeLabel = new JLabel("Mode:");
		ModeLabel.setForeground(fontColor);
		ModeLabel.setBounds(6, 118, 48, 16);
		statCalcFrame.getContentPane().add(ModeLabel);
		
		JLabel rangeLabel = new JLabel("Range:");
		rangeLabel.setForeground(fontColor);
		rangeLabel.setBounds(6, 238, 61, 16);
		statCalcFrame.getContentPane().add(rangeLabel);
		
		JLabel medianLabel = new JLabel("Median:");
		medianLabel.setForeground(fontColor);
		medianLabel.setBounds(6, 172, 61, 16);
		statCalcFrame.getContentPane().add(medianLabel);
		
		//Panels and Text Fields
		input = new JTextField();
		input.setBackground(windowColor2);
		input.setForeground(fontColor);
		input.setBounds(6, 24, 314, 26);
		statCalcFrame.getContentPane().add(input);
		input.setColumns(10);
		
		JEditorPane inputPane = new JEditorPane();
		inputPane.setBounds(6, 23, 314, 27);
		statCalcFrame.getContentPane().add(inputPane);
		
		meanText = new JTextField();
		meanText.setBackground(windowColor2);
		meanText.setForeground(fontColor);
		meanText.setHorizontalAlignment(SwingConstants.TRAILING);
		meanText.setColumns(10);
		meanText.setBounds(6, 79, 158, 26);
		statCalcFrame.getContentPane().add(meanText);
		
		JEditorPane meanPane = new JEditorPane();
		meanPane.setBounds(6, 79, 158, 27);
		statCalcFrame.getContentPane().add(meanPane);
		
		modeText = new JTextField();
		modeText.setBackground(windowColor2);
		modeText.setForeground(fontColor);
		modeText.setHorizontalAlignment(SwingConstants.TRAILING);
		modeText.setColumns(10);
		modeText.setBounds(6, 134, 158, 26);
		statCalcFrame.getContentPane().add(modeText);
		
		JEditorPane modePane = new JEditorPane();
		modePane.setBounds(6, 131, 158, 27);
		statCalcFrame.getContentPane().add(modePane);
		
		medianText = new JTextField();
		medianText.setBackground(windowColor2);
		medianText.setForeground(fontColor);
		medianText.setHorizontalAlignment(SwingConstants.TRAILING);
		medianText.setColumns(10);
		medianText.setBounds(6, 190, 158, 26);
		statCalcFrame.getContentPane().add(medianText);
		
		JEditorPane medianPane = new JEditorPane();
		medianPane.setBounds(6, 189, 158, 27);
		statCalcFrame.getContentPane().add(medianPane);
		
		rangeText = new JTextField();
		rangeText.setBackground(windowColor2);
		rangeText.setForeground(fontColor);
		rangeText.setHorizontalAlignment(SwingConstants.TRAILING);
		rangeText.setColumns(10);
		rangeText.setBounds(6, 255, 158, 26);
		statCalcFrame.getContentPane().add(rangeText);
		
		JEditorPane rangePane = new JEditorPane();
		rangePane.setBounds(6, 254, 158, 27);
		statCalcFrame.getContentPane().add(rangePane);
		
		//Buttons
		JButton executeButton = new JButton("Execute");
		executeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		executeButton.setBounds(6, 293, 158, 29);
		statCalcFrame.getContentPane().add(executeButton);
		
		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		resetButton.setBounds(164, 293, 156, 29);
		statCalcFrame.getContentPane().add(resetButton);
	}
}
