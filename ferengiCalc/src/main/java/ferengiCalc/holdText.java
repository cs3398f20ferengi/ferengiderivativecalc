package ferengiCalc;

public class holdText {
	/* 
	 * Stores user input into string variable for later use
	 * depending on how we want to proceed with the input
	 * The GUI will call setInput to set variable and
	 * will call getResult to display result.
	 */
	
	private String input;
	private String shuntingYardResult; // DELETE ME testing
	private double result; 
	ShuntingYard shuntingYard = new ShuntingYard();
	
	public void setInput(String input) {
		this.input = input;
		/* takes input from GUI, sends to shunting yard, 
		 * result of shunting yard is computed in evalRPN 
		 * and variable "result" is set.
		 * GUI calls getResult() and result is displayed
		 */
		shuntingYardResult = ShuntingYard.makePF(input);
		System.out.println(shuntingYardResult); //TESTING
		setResult(ComputeShuntingYard.evalRPN(shuntingYardResult)); 
	}
	
	public void setResult(double result) {
		this.result = result;
	}
	
	public String getInput() {
		return input;
	}

	public double getResult() {
		//will display into GUI as result;
		return result;
	}
}
// test comment
