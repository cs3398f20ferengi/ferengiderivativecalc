package ferengiCalc;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class BasicCalculatorTest {
	
	@Test
	
	// Tests addition
		void additionTest() {
		double num1 = 5;
		double num2 = 10;
		double answer = num1 + num2;
			assertEquals(15, answer );
					}
	@Test
	// Tests subtraction
	void subtractionTest() {
		double num1 = 50;
		double num2 = 5;
		double answer = num1 - num2;
			assertEquals(45, answer);
					}
	@Test
	// Tests multiplication
	void multiplicationTest() {
		double num1 = 10;
		double num2 = 10;
		double answer = num1 * num2;
			assertEquals(100, answer);
					}
	@Test
	// Tests division
	void divisionTest() {
		
		double num1 = 20;
		double num2 = 4;
		double answer = num1 / num2;
			assertEquals(5, answer);
					}
                    
                    
                    
                    
                    
                    
}