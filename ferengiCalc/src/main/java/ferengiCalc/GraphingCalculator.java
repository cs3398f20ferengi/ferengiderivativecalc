package ferengiCalc;


import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.swing.*;

public class GraphingCalculator {

	GraphingCalculator() {
		final JFrame frame = new JFrame("Graphing Calculator");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(650,500);
		final JFXPanel fxpanel = new JFXPanel();
		frame.add(fxpanel);
		
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				
			    WebEngine engine;
			    WebView wv = new WebView();
			    engine = wv.getEngine();
			    fxpanel.setScene(new Scene(wv));
			    
			    //Tomcat API script
			    //engine.load("http://localhost:8080/GraphingAPI/desmosScript.html");
			    
			    //From external drive
			    engine.load("file:///G:/desmos.html");
			    //engine.load("file://ferengiCalc/desmos.html");
			}
			
		});
		
		frame.setVisible(true);
	}
}
