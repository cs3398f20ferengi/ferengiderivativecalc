package ferengiCalc;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JEditorPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Arrays;

public class statCalculator extends customAbstract {

    JFrame statCalcFrame;
	private JTextField value2;
	private JLabel value1;
	statCompute SC = new statCompute();
    	
        
        
     public static void statCalc() {
          EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					statCalculator window = new statCalculator();
					window.basicCalcFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
        }
        
 	public statCalculator() {
		initialize();
	}

     /**
	 * Initialize the contents of the frame.
	 */
     
	private JLabel calcAction;
	private void initialize() {
		statCalcFrame = new JFrame();
		statCalcFrame.getContentPane().setBackground(windowColor1);
		statCalcFrame.setTitle("Statistic Calculator");
		statCalcFrame.setBounds(100, 100, 326, 365);
		statCalcFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		statCalcFrame.getContentPane().setLayout(null);     
        

// Calculate Mean ,Median, Mode and Range
public class statCalculation {
     JTextField textField = new JTextField(15);
     JLabel meanLbl = new JLabel("");
     JLabel medianLbl = new JLabel("");
     JLabel modeLbl = new JLabel("");
     Jlabel rangeLbl = new jLabel("");

// converting string array to int

public int[] convertArr(String arr[], int numberList){
               int[] numArr = new int[n]; 
           
           for(int i= 0;i<n;i++){
               numArr[i] = Integer.parseInt(arr[i]);
                   }
                   return numArr;
  }

//get mean
public double getMean(double[] numberList) {
    double total;
    for (double d: numberList) {
        total += d;
    }
    return total / (numberList.length);
}
//get median
public double getMedian(double[] numberList) {
    Arrays.sort(numberlist);
     int factor = numberList.length - 1;
    double[] first = new double[(double) factor / 2];
    double[] last = new double[first.length];
    double[] middleNumbers = new double[1];
    for (int i = 0; i < first.length; i++) {
        first[i] = numbersList[i];
    }
    for (int i = numberList.length; i > last.length; i--) {
        last[i] = numbersList[i];
    }
    for (int i = 0; i <= numberList.length; i++) {
        if (numberList[i] != first[i] || numberList[i] != last[i]) middleNumbers[i] = numberList[i];
    }
    if (numberList.length % 2 == 0) {
        double total = middleNumbers[0] + middleNumbers[1];
        return total / 2;
    } else {
        return middleNumbers[0];
    }
}
//get mode
public double getMode(double[] numberList) {
    HashMap<Double,Double> freqs = new HashMap<Double,Double>();
    for (double d: numberList) {
        Double freq = freqs.get(d);
        freqs.put(d, (freq == null ? 1 : freq + 1));   
    }
    double mode = 0;
    double maxFreq = 0;    
    for (Map.Entry<Double,Doubler> entry : freqs.entrySet()) {     
        double freq = entry.getValue();
        if (freq > maxFreq) {
            maxFreq = freq;
            mode = entry.getKey();
        }
    }    
    return mode;
}
//get range
public double getRange(double[] numberList) {
    Arrays.sort(numberlist);
     double initMin = numberList[0];
    double initMax = numberList[0];
    for (int i = 1; i <= numberList.length; i++) {
        if (numberList[i] < initMin) initMin = numberList[i];
        if (numberList[i] > initMax) initMax = numberList[i];
    }
    return initMax - initMin;
}  
}}