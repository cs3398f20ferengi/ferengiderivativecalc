package ferengiCalc;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class trigGraph {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void trigGraphing(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					trigGraph window = new trigGraph();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public trigGraph() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 378, 503);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(10, 11, 342, 71);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Sin(x)");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String number1 = textField.getText();
				double dnum = Double.parseDouble(number1);
				double sin = Math.sin(dnum);
				String s = String.valueOf(sin);
				textField.setText(s);
						
			}
		});
		btnNewButton.setBounds(10, 299, 257, 44);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cos(x)");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String number1 = textField.getText();
				double dnum = Double.parseDouble(number1);
				double sin = Math.cos(dnum);
				String s = String.valueOf(sin);
				textField.setText(s);
			}
		});
		btnNewButton_1.setBounds(10, 354, 257, 44);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Tan(x)");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String number1 = textField.getText();
				double dnum = Double.parseDouble(number1);
				double sin = Math.tan(dnum);
				String s = String.valueOf(sin);
				textField.setText(s);
			}
		});
		btnNewButton_2.setBounds(10, 409, 257, 44);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Graph");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_3.setBounds(277, 229, 75, 99);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("C");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(null);
			}
		});
		btnNewButton_4.setBounds(277, 339, 75, 114);
		frame.getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("9");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_5.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_5.setBounds(10, 229, 87, 59);
		frame.getContentPane().add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("0");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_6.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_6.setBounds(107, 229, 75, 59);
		frame.getContentPane().add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton(".");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String inputNum = textField.getText()+btnNewButton_7.getText();
			textField.setText(inputNum);
			}
		});
		btnNewButton_7.setBounds(192, 229, 75, 59);
		frame.getContentPane().add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("5");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_8.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_8.setBounds(10, 159, 87, 59);
		frame.getContentPane().add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("6");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_9.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_9.setBounds(107, 159, 75, 59);
		frame.getContentPane().add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("7");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_10.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_10.setBounds(192, 159, 75, 59);
		frame.getContentPane().add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("8");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_11.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_11.setBounds(277, 159, 75, 59);
		frame.getContentPane().add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("1");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_12.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_12.setBounds(10, 93, 87, 55);
		frame.getContentPane().add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton("2");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_13.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_13.setBounds(107, 93, 75, 55);
		frame.getContentPane().add(btnNewButton_13);
		
		JButton btnNewButton_14 = new JButton("3");
		btnNewButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_14.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_14.setBounds(192, 93, 75, 55);
		frame.getContentPane().add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("4");
		btnNewButton_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputNum = textField.getText()+btnNewButton_15.getText();
				textField.setText(inputNum);
			}
		});
		btnNewButton_15.setBounds(277, 93, 75, 55);
		frame.getContentPane().add(btnNewButton_15);
	}
}