package ferengiCalc;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;


/*Resources:
 * 
 * https://en.wikipedia.org/wiki/Shunting-yard_algorithm
 * https://www.javacodegeeks.com/2017/09/guide-parsing-algorithms-terminology.html
 *
 */


public class ShuntingYard {

	//set values to operators to find precedence
		private enum Operator{
			POWER(4), MULT(3), DIV(3), ADD(2), SUB(2);
			final int precedence;
			Operator(int p){
				precedence = p;
			}
		}
		
		//set string equal to corresponding operator
		@SuppressWarnings("serial")
		private static Map<String, Operator> opMap = new HashMap<String, Operator>() {{
			put("^", Operator.POWER);
			put("*", Operator.MULT);
			put("/", Operator.DIV);
			put("+", Operator.ADD);
			put("-", Operator.SUB);
			
		}};
		
		//sort operator precedence before pushing appending
		private static Boolean getPrecedence(String op, String op2) {
			return (opMap.containsKey(op2) && opMap.get(op2).precedence >= opMap.get(op).precedence);
		} 
		
		public static String makePF(String input) {
			StringBuilder postfix = new StringBuilder();
			Stack<String> ops = new Stack<String>();
			
			for(String token : input.split(" ")) {
				//append operators( +, - , ^, /, *)
				if(opMap.containsKey(token)) {
					while(!ops.isEmpty() && getPrecedence(token, ops.peek())) {
						postfix.append(ops.pop()).append(' ');
					}
					ops.push(token);
				}
				//push open parenthesis
				else if(token.equals("(")) {
					ops.push(token);
				}
				//push close parenthesis and append open parenthesis
				else if(token.equals(")")) {
					while(!ops.peek().equals("(")) {
						postfix.append(ops.pop()).append(' ');
					}
					ops.pop();
				}
				//append digits and variables
				else {
					postfix.append(token).append(' ');
				}
			}
			//append until stack is empty
			while(!ops.isEmpty()) {
				postfix.append(ops.pop()).append(' ');
			}
			//return the as string
			return postfix.toString();
		}
	
	
}

