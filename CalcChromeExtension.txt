Java files are no longer supported by Google Chrome. Instead we must use Google web Toolkit,
which is an eclipse plug-in, to compile our Java into Javascript. 

GWT Documentation: http://www.gwtproject.org/doc/latest/DevGuide.html
GWT Java Docs: http://www.gwtproject.org/javadoc/latest/
GWT Tutorials: http://www.gwtproject.org/doc/latest/tutorial/index.html
Plugin for Eclipse: https://cloud.google.com/eclipse/docs/quickstart

Porting our application would require some experience in Javascript.


