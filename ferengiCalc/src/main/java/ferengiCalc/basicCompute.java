package ferengiCalc;

public class basicCompute {

	private String str1;
	private String str2;
	private String strProblem;
	private String operator;
	private double num1;
	private double num2;
	private double result;
		
	public void setValues1(String str1) {
		this.str1 = str1;
		num1 = Double.parseDouble(str1);
	}
	
	public void setValues2(String str2) {
		this.str2 = str2;
		num2 = Double.parseDouble(str2);
	}
	
	public void setOperation(String operator) {
		this.operator = operator;
		setStringProblem();
		calculate();
	}
	
	public void setStringProblem() {
		strProblem = str1 + " " + operator + " " + str2 + " =";
	}
	
	public String getNumResult() {
		return String.valueOf(result);
	}
	
	public String getProblem() {
		return strProblem;
	}
	
	private void calculate() {
		switch(operator) {
		case "-": 
			result = num1 - num2; 
			break;
			
		case "+":
			result = num1 + num2;
			break;
			
		case "x":
			result = num1 * num2;
			break;
			
		case "/":
			result = num1 / num2;
			break;
		//default;	
		
		}
		
	}
}