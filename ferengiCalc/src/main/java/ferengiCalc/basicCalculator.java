package ferengiCalc;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
//import javax.swing.JEditorPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
//import java.awt.Color;

public class basicCalculator extends customAbstract {

	JFrame basicCalcFrame;
	private JTextField value2;
	private JLabel value1;
	basicCompute bC = new basicCompute();

	/**
	 * Launch the application.
	 */
	public static void basicCalc() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					basicCalculator window = new basicCalculator();
					window.basicCalcFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public basicCalculator() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private JLabel calcAction;
	private void initialize() {
		basicCalcFrame = new JFrame();
		basicCalcFrame.getContentPane().setBackground(windowColor1);
		basicCalcFrame.setTitle("Standard Calculator");
		basicCalcFrame.setBounds(100, 100, 365, 500);
		basicCalcFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		basicCalcFrame.getContentPane().setLayout(null);
		
		value1 = new JLabel();
		value1.setBackground(windowColor2);
		value1.setForeground(fontColor);
		value1.setHorizontalAlignment(SwingConstants.TRAILING);
		value1.setBounds(5, 5, 330, 35);
		basicCalcFrame.getContentPane().add(value1);
		
		value2 = new JTextField();
		value2.setBackground(windowColor2);
		value2.setForeground(fontColor);
		value2.setHorizontalAlignment(SwingConstants.TRAILING);
		value2.setBounds(5, 45, 340, 35);
		basicCalcFrame.getContentPane().add(value2);
		value2.setColumns(10);
		
		//JEditorPane editorPane = new JEditorPane();
		//editorPane.setBounds(6, 6, 338, 77);
		//basicCalcFrame.getContentPane().add(editorPane);
		
		JButton button_0 = new JButton("0");
		button_0.setForeground(fontColor);
		button_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 0);
			}
		});
		button_0.setBounds(94, 323, 76, 64);
		basicCalcFrame.getContentPane().add(button_0);
		
		JButton button_1 = new JButton("1");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 1);
			}
		});
		button_1.setBounds(6, 247, 76, 64);
		basicCalcFrame.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("2");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 2);
			}
		});
		button_2.setBounds(94, 247, 76, 64);
		basicCalcFrame.getContentPane().add(button_2);
		
		JButton button_3 = new JButton("3");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 3);
			}
		});
		button_3.setBounds(182, 247, 76, 64);
		basicCalcFrame.getContentPane().add(button_3);
		
		JButton button_4 = new JButton("4");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 4);
			}
		});
		button_4.setBounds(6, 171, 76, 64);
		basicCalcFrame.getContentPane().add(button_4);
		
		JButton button_5 = new JButton("5");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 5);
			}
		});
		button_5.setBounds(94, 171, 76, 64);
		basicCalcFrame.getContentPane().add(button_5);
		
		JButton button_6 = new JButton("6");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 6);
			}
		});
		button_6.setBounds(182, 171, 76, 64);
		basicCalcFrame.getContentPane().add(button_6);
		
		JButton button_7 = new JButton("7");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 7);
			}
		});
		button_7.setBounds(6, 95, 76, 64);
		basicCalcFrame.getContentPane().add(button_7);
		
		JButton button_8 = new JButton("8");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 8);
			}
		});
		button_8.setBounds(94, 95, 76, 64);
		basicCalcFrame.getContentPane().add(button_8);
		
		JButton button_9 = new JButton("9");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText(value2.getText() + 9);
			}
		});
		button_9.setBounds(182, 95, 76, 64);
		basicCalcFrame.getContentPane().add(button_9);
		
		JButton button_neg = new JButton("+/-");
		button_neg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value2.setText("-" + value2.getText());
			}
		});
		button_neg.setBounds(6, 323, 76, 64);
		basicCalcFrame.getContentPane().add(button_neg);
		
		JButton button_plus = new JButton("+");
		button_plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value1.setText(value2.getText());
				calcAction.setText("+");
				value2.setText(null);
			}
		});
		button_plus.setBounds(268, 95, 76, 64);
		basicCalcFrame.getContentPane().add(button_plus);
		
		JButton button_minus = new JButton("-");
		button_minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value1.setText(value2.getText());
				calcAction.setText("-");
				value2.setText(null);
			}
		});
		button_minus.setBounds(268, 171, 76, 64);
		basicCalcFrame.getContentPane().add(button_minus);
		
		JButton button_multiple = new JButton("x");
		button_multiple.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value1.setText(value2.getText());
				calcAction.setText("*");
				value2.setText(null);
			}
		});
		button_multiple.setBounds(268, 247, 76, 64);
		basicCalcFrame.getContentPane().add(button_multiple);
		
		JButton button_divide = new JButton("/");
		button_divide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value1.setText(value2.getText());
				calcAction.setText("/");
				value2.setText(null);
			}
		});
		button_divide.setBounds(268, 323, 76, 64);
		basicCalcFrame.getContentPane().add(button_divide);
		
		JButton button_equals = new JButton("=");
		button_equals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//----------------------------------------------
				//sends to basicCompute class and returns answer
				//----------------------------------------------
				bC.setValues1(value1.getText());
				bC.setValues2(value2.getText());
				bC.setOperation(calcAction.getText());
				value1.setText(bC.getProblem());
				value2.setText(bC.getNumResult());
			}
		});
		
		button_equals.setBounds(6, 389, 338, 64);
		basicCalcFrame.getContentPane().add(button_equals);
		
		JButton btnC = new JButton("Clear");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				value1.setText(null);
				value2.setText(null);
				calcAction.setText(null);
			}
		});
		btnC.setBounds(182, 323, 76, 64);
		basicCalcFrame.getContentPane().add(btnC);
		
		calcAction = new JLabel("");
	}
}


