package ferengiCalc;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

public class customFrame {

	private JFrame customFrame;

	/**
	 * Launch the application.
	 */
	public static void customWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					customFrame window = new customFrame();
					window.customFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public customFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		customFrame = new JFrame();
		customFrame.setTitle("Settings");
		customFrame.setBounds(100, 100, 250, 190);
		customFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		customFrame.getContentPane().setLayout(null);
		
		JLabel customizeLabel = new JLabel("Choose Display:");
		customizeLabel.setBounds(6, 6, 160, 16);
		customFrame.getContentPane().add(customizeLabel);
		
		
		//Default Button
		JButton defaultButton = new JButton("Default");
		defaultButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customAbstract.windowColor1 = Color.white;
				customAbstract.windowColor2 = Color.white;
				customAbstract.fontColor = Color.black;
				System.out.println("Display changed to 'Default'");
			}
		});
		defaultButton.setBounds(6, 34, 117, 29);
		customFrame.getContentPane().add(defaultButton);
		
		//Matrix Button
		JButton matrixButton = new JButton("Matrix");
		matrixButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customAbstract.windowColor1 = Color.black;
				customAbstract.windowColor2 = Color.black;
				customAbstract.fontColor = Color.green;
				System.out.println("Display changed to 'Matrix'");
			}
		});
		matrixButton.setBounds(6, 63, 117, 29);
		customFrame.getContentPane().add(matrixButton);
		
		//coming soon button
		JButton beeButton = new JButton("Bee");
		beeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customAbstract.windowColor1 = Color.black;
				customAbstract.windowColor2 = Color.black;
				customAbstract.fontColor = Color.yellow;
				System.out.println("Display changed to 'Bee'");
			}
		});
		beeButton.setBounds(6, 92, 117, 29);
		customFrame.getContentPane().add(beeButton);
		
		//coming soon button
		JButton redBlueButton = new JButton("Red and Blue");
		redBlueButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customAbstract.windowColor1 = Color.red;
				customAbstract.windowColor2 = Color.white;
				customAbstract.fontColor = Color.blue;
			}
		});
		redBlueButton.setBounds(127, 34, 117, 29);
		customFrame.getContentPane().add(redBlueButton);
		
		//coming soon button
		JButton bluesButton = new JButton("Blues");
		bluesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customAbstract.windowColor1 = Color.cyan;
				customAbstract.windowColor2 = Color.white;
				customAbstract.fontColor = Color.blue;
			}
		});
		bluesButton.setBounds(127, 63, 117, 29);
		customFrame.getContentPane().add(bluesButton);
		
		//coming soon button
		JButton orangeButton = new JButton("Orange");
		orangeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customAbstract.windowColor1 = Color.orange;
				customAbstract.windowColor2 = Color.white;
				customAbstract.fontColor = Color.black;
			}
		});
		orangeButton.setBounds(127, 92, 117, 29);
		customFrame.getContentPane().add(orangeButton);
	}

}