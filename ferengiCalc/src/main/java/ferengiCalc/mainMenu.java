package ferengiCalc;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class mainMenu {

	private JFrame mainMenuFrame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mainMenu window = new mainMenu();
					window.mainMenuFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public mainMenu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		mainMenuFrame = new JFrame();
		mainMenuFrame.setTitle("Main Menu");
		//mainMenuFrame.setBounds(100, 100, 250, 200);
		mainMenuFrame.setBounds(100, 110, 250, 300);
		mainMenuFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainMenuFrame.getContentPane().setLayout(null);
		
		JLabel titleMenu = new JLabel("Select Calculator:");
		titleMenu.setBounds(50, 5, 150, 25);
		mainMenuFrame.getContentPane().add(titleMenu);
		
		
		//---------------Basic Calc--------------------
		JButton basicCalcButton = new JButton("Basic");
		basicCalcButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new basicCalculator();
				basicCalculator.basicCalc();
			}
		});
		basicCalcButton.setBounds(50, 30, 150, 30);
		mainMenuFrame.getContentPane().add(basicCalcButton);
		
		//----------------Derivative Calc---------------------
		JButton derivCalcButton = new JButton("Derivative");
		derivCalcButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new derivativeCalculator();
				derivativeCalculator.derivCalc();
			}
		});
		derivCalcButton.setBounds(50, 60, 150, 30);
		mainMenuFrame.getContentPane().add(derivCalcButton);
		
		//-------------------Stats Calc------------------------
		JButton statCalcButton = new JButton("Statistic");
		statCalcButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new statCalculator();
				statCalculator.statCalc();
			}
		});
		statCalcButton.setBounds(50, 90, 150, 30);
		mainMenuFrame.getContentPane().add(statCalcButton);
		
		
		//---------------------Graphing Calc-------------------
		JButton graphCalcButton = new JButton("Graphing");
		graphCalcButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GraphingCalculator();
			}
		});
		graphCalcButton.setBounds(50, 120, 150, 30);
		mainMenuFrame.getContentPane().add(graphCalcButton);
		
		//-------------------Settings--------------------------
		JButton settingsButton = new JButton("Settings");
		settingsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new customFrame();
				customFrame.customWindow();
			}
		});
		settingsButton.setBounds(50, 210, 150, 30);
		mainMenuFrame.getContentPane().add(settingsButton);
		
		//---------------------Trig Calc-------------------
		JButton trigButton = new JButton("Trig. Functions");
		trigButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trigWindow nw = new trigWindow();
				nw.trigScreen(null);
				//trigCalculator.trigCalc();
			}
		});
		trigButton.setBounds(50, 150, 150, 30);
		mainMenuFrame.getContentPane().add(trigButton);
		
		//---------------------Trig Graphing-------------------
		JButton trigGraphingButton = new JButton("Trig. Graphing");
		trigGraphingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trigGraph nw = new trigGraph();
				nw.trigGraphing(null);
				//trigCalculator.trigCalc();
			}
		});
		trigGraphingButton.setBounds(50, 180, 150, 30);
		mainMenuFrame.getContentPane().add(trigGraphingButton);
	}
}
