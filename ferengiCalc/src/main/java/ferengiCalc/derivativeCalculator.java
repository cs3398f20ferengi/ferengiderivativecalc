package ferengiCalc;


import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class derivativeCalculator extends customAbstract {

	JFrame derivCalcFrame;
	private JTextField input;
	private JTextField answer;
	private JTextField xInput;
	
	//Launch the application.
	public static void derivCalc() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					derivativeCalculator window = new derivativeCalculator();
					window.derivCalcFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Create the application.
	public derivativeCalculator() {
		initialize();
	}

	//Initialize the contents of the frame.
	private void initialize() {
		holdText ht = new holdText();
		
		derivCalcFrame = new JFrame();
		derivCalcFrame.getContentPane().setBackground(windowColor1);
		derivCalcFrame.setTitle("Derivative Calculator");
		derivCalcFrame.setBounds(100, 100, 340, 239);
		derivCalcFrame.setBackground(null);
		derivCalcFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		derivCalcFrame.getContentPane().setLayout(null);
		derivCalcFrame.setBackground(windowColor1);
		
		//Panel info
		input = new JTextField();
		input.setBackground(windowColor2);
		input.setForeground(fontColor);
		input.setHorizontalAlignment(SwingConstants.TRAILING);
		input.setBounds(6, 25, 313, 26);
		derivCalcFrame.getContentPane().add(input);
		input.setColumns(10);
		
		JEditorPane inputPane = new JEditorPane();
		inputPane.setBounds(6, 22, 313, 29);
		derivCalcFrame.getContentPane().add(inputPane);
		
		answer = new JTextField();
		answer.setBackground(windowColor2);
		answer.setForeground(fontColor);
		answer.setHorizontalAlignment(SwingConstants.TRAILING);
		answer.setBounds(6, 143, 313, 26);
		derivCalcFrame.getContentPane().add(answer);
		answer.setColumns(10);
		
		JEditorPane answerPane = new JEditorPane();
		answerPane.setBounds(6, 140, 313, 29);
		derivCalcFrame.getContentPane().add(answerPane);
		
		xInput = new JTextField();
		xInput.setBackground(windowColor2);
		xInput.setForeground(fontColor);
		xInput.setHorizontalAlignment(SwingConstants.TRAILING);
		xInput.setBounds(6, 81, 157, 22);
		derivCalcFrame.getContentPane().add(xInput);
		xInput.setColumns(10);
		
		JEditorPane xValuePane = new JEditorPane();
		xValuePane.setBounds(6, 77, 157, 26);
		derivCalcFrame.getContentPane().add(xValuePane);
		
		//enterInput Label info
		JLabel enterInputLabel = new JLabel("Enter Formula:");
		enterInputLabel.setBounds(6, 6, 117, 16);
		enterInputLabel.setForeground(fontColor);
		derivCalcFrame.getContentPane().add(enterInputLabel);
		
		//example Label info
		JLabel exampleLabel = new JLabel("ex. ( 3 + 7 ) / 2");
		exampleLabel.setFont(new Font("Lucida Grande", Font.ITALIC, 10));
		exampleLabel.setBounds(202, 54, 117, 16);
		exampleLabel.setForeground(fontColor);
		derivCalcFrame.getContentPane().add(exampleLabel);
		
		//answer Label info
		JLabel answerLabel = new JLabel("Answer:");
		answerLabel.setBounds(6, 115, 61, 16);
		answerLabel.setForeground(fontColor);
		derivCalcFrame.getContentPane().add(answerLabel);
		
		//execute button info
		JButton executeButton = new JButton("Execute");
		executeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ht.setXValue(xInput.getText());
				ht.setInput(input.getText());
				answer.setText(String.valueOf(ht.getResult()));
			}
		});
		executeButton.setBounds(14, 182, 149, 29);
		derivCalcFrame.getContentPane().add(executeButton);
		
		//reset button info
		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input.setText("");
				answer.setText("");
			}
		});
		resetButton.setBounds(175, 182, 144, 29);
		derivCalcFrame.getContentPane().add(resetButton);
		
		//x-value label info
		JLabel enterXLabel = new JLabel("Enter x-value:");
		enterXLabel.setForeground(fontColor);
		enterXLabel.setBounds(6, 53, 117, 16);
		derivCalcFrame.getContentPane().add(enterXLabel);
	}
}